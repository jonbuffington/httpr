package main

import (
	"compress/gzip"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

// logResponseWriter wraps an http.ResponseWriter to capture bytes and
// status values for logging after a wrapped Handler is called.
type logResponseWriter struct {
	http.ResponseWriter
	bytes  int
	status int
}

func (w *logResponseWriter) Write(bytes []byte) (int, error) {
	w.bytes += len(bytes)
	return w.ResponseWriter.Write(bytes)
}

func (w *logResponseWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

// Use strftime format %d/%b/%Y:%H:%M:%S %z to output time to log.
const layout = "2/Jan/2006:15:04:05 Z0700"

// logAfterHandler returns a Handler that first calls the wrapped h Handler
// and then emits a log entry to stdout using the common log format.
func logAfterHandler(h http.Handler) http.Handler {
	wrapper := func(w http.ResponseWriter, r *http.Request) {
		lw := &logResponseWriter{w, 0, 0}

		h.ServeHTTP(lw, r)

		// See <http://en.wikipedia.org/wiki/Common_Log_Format>.
		fmt.Printf("%s - - [%s] \"%s %s %s\" %d %d\n",
			r.RemoteAddr,
			time.Now().Format(layout),
			r.Method, r.RequestURI, r.Proto,
			lw.status, lw.bytes)
	}

	return http.HandlerFunc(wrapper)
}

// gzipResponseWriter wraps an http.ResponseWriter to GZIP the response bytes.
type gzipResponseWriter struct {
	io.Writer
	http.ResponseWriter
	hasSetContentType bool
}

func (w *gzipResponseWriter) Write(b []byte) (int, error) {
	if !w.hasSetContentType {
		if w.Header().Get("Content-Type") == "" {
			w.Header().Set("Content-Type", http.DetectContentType(b))
		}
		w.hasSetContentType = true
	}
	return w.Writer.Write(b)
}

// gzipHandler returns a Handler that calls will gzip the response bytes if
// the request accepts gzip encoded responses.
func gzipHandler(h http.Handler) http.Handler {
	wrapper := func(w http.ResponseWriter, r *http.Request) {
		if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
			h.ServeHTTP(w, r)
			return
		}

		w.Header().Set("Content-Encoding", "gzip")
		w.Header().Set("Vary", "Accept-Encoding") // See http://www.fastly.com/blog/best-practices-for-using-the-vary-header/
		gz := gzip.NewWriter(w)
		defer gz.Close()

		h.ServeHTTP(&gzipResponseWriter{Writer: gz, ResponseWriter: w}, r)
	}

	return http.HandlerFunc(wrapper)
}

// Create a command line -http option that defaults to port 4000.
var addr = flag.String("http", ":4000", "HTTP service address")

func main() {
	flag.Parse()
	fmt.Printf("Listening on address %s.\n", *addr)
	http.Handle("/", logAfterHandler(gzipHandler(http.FileServer(http.Dir("public")))))
	log.Fatal(http.ListenAndServe(*addr, nil))
}
